FROM node:14.19.1-alpine3.14 AS development

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn install 

COPY . .

RUN yarn run build