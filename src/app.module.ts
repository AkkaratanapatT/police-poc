import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MetaModule } from './meta/meta.module';
import { WalkerModule } from './modules/walker.module';
import { ReportModule } from './report/report.module';

@Module({
  imports: [
    WalkerModule,
    MetaModule,
    ReportModule,
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        DIR_PATH: Joi.string().required(),
      }),
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
