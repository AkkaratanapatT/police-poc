import * as dirTree from 'directory-tree';

import { Controller, Get, Inject } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import * as klawSync from 'klaw-sync';
import { cwd } from 'process';
import walkerConfig from './walker.config';

@Controller('walker')
export class WalkerController {
  constructor(
    @Inject(walkerConfig.KEY)
    private readonly config: ConfigType<typeof walkerConfig>,
  ) {}

  @Get('file-paths')
  filePaths() {
    const path = `${this.config.dirPath}`;
    return dirTree(path, {
      extensions: /\.(h264|n3r|dvr|dav|sec|av|avi|mp4)$/,
      attributes: [
        'type',
        'extension',
        // 'dev',
        // 'ino',
        // 'mode',
        // 'nlink',
        // 'uid',
        // 'gid',
        // 'rdev',
        // 'size',
        // 'blksize',
        // 'blocks',
        // 'atimeMs',
        // 'mtimeMs',
        // 'ctimeMs',
        // 'birthtimeMs',
        // 'atime',
        // 'mtime',
        // 'ctime',
      ],
    });
  }
}
