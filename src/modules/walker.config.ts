import { registerAs } from '@nestjs/config';

export default registerAs('walker', () => ({
  dirPath: process.env.DIR_PATH,
}));
