import * as Joi from 'joi';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { WalkerController } from './walker.controller';
import walkerConfig from './walker.config';

@Module({
  imports: [ConfigModule.forFeature(walkerConfig)],
  controllers: [WalkerController],
})
export class WalkerModule {}
