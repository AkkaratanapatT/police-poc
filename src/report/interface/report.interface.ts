export interface Report {
  id?: string;
  path: string;
  case: Case[];
  location: string;
}

interface Case {
  id?: string;
  case: string;
  snapshot: string;
  startDate: string; // iso string
  endDate: string; // iso string
}
