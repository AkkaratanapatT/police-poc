import { Injectable } from '@nestjs/common';
import { Report } from './interface/report.interface';  
 
@Injectable()
export default class ReportService {
  private readonly reportList: Report[] = [
    {
      id: 'ce3e5485-71de-4765-b455-ed65b93d64620',
      path: 'local/video/5-03-2022.mp4',
      location: 'S Sathorn Rd',
      case: [
        {
          id: 'we3e5485-71de-4765-b455-ed65b93d64620',
          startDate: '2022-03-22T09:01:57.791Z',
          endDate: '2022-03-22T09:05:57.791Z',
          case: 'human',
          snapshot: 'https://i.ibb.co/0C3G0j9/1.jpg',
        },
        {
          id: 'we3e5485-71de-4765-b455-ed65b93d64621',
          startDate: '2022-03-22T10:00:57.791Z',
          endDate: '2022-03-22T10:07:57.791Z',
          case: 'human',
          snapshot: 'https://i.ibb.co/1r6fy4p/2.webp',
        },
      ],
    },
    {
      id: 'ce3e5485-71de-4765-b455-ed65b93d64622',
      path: 'local/video/6-03-2022.mp4',
      location: 'Thanon Naradhiwat Rajanagarindra',
      case: [
        {
          id: 'we3e5485-71de-4765-b455-ed65b93d64622',
          startDate: '2022-03-22T10:00:57.791Z',
          endDate: '2022-03-22T10:05:57.791Z',
          case: 'car',
          snapshot: 'https://i.ibb.co/ctb76rj/9.png',
        },
      ],
    },
    {
      id: 'ce3e5485-71de-4765-b455-ed65b93d64623',
      path: 'local/video/6-03-2022.mp4',
      location: 'Thanon Naradhiwat Rajanagarindra',
      case: [
        {
          id: 'we3e5485-71de-4765-b455-ed65b93d64623',
          startDate: '2022-03-22T09:00:57.791Z',
          endDate: '2022-03-22T09:13:57.791Z',
          case: 'human',
          snapshot: 'https://i.ibb.co/9VNs3vs/7.jpg',
        },
      ],
    },
    {
      id: 'ce3e5485-71de-4765-b455-ed65b93d64624',
      path: 'local/video/5-03-2022.mp4',
      location: 'S Sathorn Rd',
      case: [
        {
          id: 'we3e5485-71de-4765-b455-ed65b93d64624',
          startDate: '2022-03-22T09:23:57.791Z',
          endDate: '2022-03-22T09:30:57.791Z',
          case: 'motorcycle',
          snapshot: 'https://i.ibb.co/Ycj9Rhh/6.jpg',
        },
      ],
    },
    {
      id: 'ce3e5485-71de-4765-b455-ed65b93d64625',
      path: 'local/video/5-03-2022.mp4',
      location: 'S Sathorn Rd',
      case: [
        {
          id: 'we3e5485-71de-4765-b455-ed65b93d64625',
          startDate: '2022-03-22T09:25:57.791Z',
          endDate: '2022-03-22T09:28:57.791Z',
          case: 'car',
          snapshot: 'https://i.ibb.co/Ky2RtSd/5.png',
        },
        {
          id: 'we3e5485-71de-4765-b455-ed65b93d64625',
          startDate: '2022-03-22T09:40:57.791Z',
          endDate: '2022-03-22T09:50:57.791Z',
          case: 'car',
          snapshot: 'https://i.ibb.co/1880w13/4.png',
        },
      ],
    },
    {
      id: 'ce3e5485-71de-4765-b455-ed65b93d64626',
      path: 'local/video/6-03-2022.mp4',
      location: 'Thanon Naradhiwat Rajanagarindra',
      case: [
        {
          id: 'we3e5485-71de-4765-b455-ed65b93d64626',
          startDate: '2022-03-22T09:34:57.791Z',
          endDate: '2022-03-22T09:38:57.791Z',
          case: 'motorcycle',
          snapshot: 'https://i.ibb.co/FzM5z3W/3.png',
        },
      ],
    },
  ];

  getReport() {
    return {
      msg: 'ok',
      report: this.reportList,
    };
  }
}
