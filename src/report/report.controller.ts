import { BadRequestException, Body, Controller, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Query } from "@nestjs/common";
import ReportService from "./report.service";

@Controller('report')
export default class ReportController {

    constructor(private reportService: ReportService) {

    }

    @Get()
    getReport() {
        return this.reportService.getReport();
    }

    @Post()
    createOne(@Body() body: any) {
        console.log(body);
        return true;
    }
}