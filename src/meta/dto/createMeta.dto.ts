import { IsString, IsInt, IsJSON } from 'class-validator';

export class CreateMetaDto {
  @IsString()
  path: string;
  // @IsJSON()
  meta: any;
}

export default CreateMetaDto;