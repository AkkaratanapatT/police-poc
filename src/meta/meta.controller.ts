import {
  Body,
  Controller,
  Get,
  Post,
} from '@nestjs/common';
import CreateMetaDto from './dto/createMeta.dto';
import MetaService from './meta.service';

@Controller('meta')
export default class CatController {
  constructor(private metaService: MetaService) {}

  @Get(':id')
  getOne() {
    return this.metaService.getMeta();
  }

  @Get()
  getMeta() {
    return this.metaService.getMeta();
  }

  @Post()
  createOne(@Body() meta: CreateMetaDto) {
    return this.metaService.addMeta(meta);
  }
}
