import { Injectable } from '@nestjs/common';
import { Meta } from './interface/meta.interface';

@Injectable()
export default class MetaService {
  // stateful service for mocking
  private readonly metaList: Meta[] = [
    {
      id: 'ae3e5485-71de-4765-b455-ed65b93d6462',
      path: 'local/video/5-03-2022.mp4',
      meta: {
        lat: '13.782088',
        lon: '100.415823',
        location: 'Camera 1',
        startDate: '2022-03-22T09:00:57.791Z',
        endDate: '2022-03-23T09:00:57.791Z',
      },
    },
    {
      id: 'ae3e5485-71de-4765-b455-ed65b93d6463',
      path: 'local/video/6-03-2022.mp4',
      meta: {
        lat: '13.781966',
        lon: '100.414996',
        location: 'Camera 2',
        startDate: '2022-03-22T09:00:57.791Z',
        endDate: '2022-03-23T09:00:57.791Z',
      },
    },
    {
      id: 'ae3e5485-71de-4765-b455-ed65b93d6464',
      path: 'local/video/7-03-2022.mp4',
      meta: {
        lat: '13.781819',
        lon: '100.415024',
        location: 'Camera 3',
        startDate: '2022-03-22T09:00:57.791Z',
        endDate: '2022-03-23T09:00:57.791Z',
      },
    },
    {
      id: 'ae3e5485-71de-4765-b455-ed65b93d6465',
      path: 'local/video/8-03-2022.mp4',
      meta: {
        lat: '13.782179',
        lon: '100.414921',
        location: 'Camera 4',
        startDate: '2022-03-22T09:00:57.791Z',
        endDate: '2022-03-23T09:00:57.791Z',
      },
    },
    {
      id: 'ae3e5485-71de-4765-b455-ed65b93d6466',
      path: 'local/video/9-03-2022.mp4',
      meta: {
        lat: '13.781767',
        lon: '100.414954',
        location: 'Camera 5',
        startDate: '2022-03-22T09:00:57.791Z',
        endDate: '2022-03-23T09:00:57.791Z',
      },
    },
    {
      id: 'ae3e5485-71de-4765-b455-ed65b93d6467',
      path: 'local/video/10-03-2022.mp4',
      meta: {
        lat: '13.781821',
        lon: '100.414863',
        location: 'Camera 6',
        startDate: '2022-03-22T09:00:57.791Z',
        endDate: '2022-03-23T09:00:57.791Z',
      },
    },
    {
      id: 'ae3e5485-71de-4765-b455-ed65b93d6468',
      path: 'local/video/11-03-2022.mp4',
      meta: {
        lat: '13.781966',
        lon: '100.414810',
        location: 'Camera 7',
        startDate: '2022-03-22T09:00:57.791Z',
        endDate: '2022-03-23T09:00:57.791Z',
      },
    },
    {
      id: 'ae3e5485-71de-4765-b455-ed65b93d6469',
      path: 'local/video/12-03-2022.mp4',
      meta: {
        lat: '13.782127',
        lon: '100.414779',
        location: 'Camera 8',
        startDate: '2022-03-22T09:00:57.791Z',
        endDate: '2022-03-23T09:00:57.791Z',
      },
    },
  ];

  getMeta() {
    return {
      msg: 'ok',
      list: this.metaList,
    };
  }

  addMeta(meta: Meta) {
    const isExist = this.metaList.find((element) => element.path == meta.path);
    if (isExist) {
      return { msg: 'path is already exist.' };
    } else {
      meta.id = new Date().getTime().toString();
      this.metaList.push(meta);
      return { msg: 'success' };
    }
  }
}
